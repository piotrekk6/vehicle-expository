export class Vehicle{
  id: number;
  name: string;
  vehicleType: string;
  model: string;
  color: string;
  productionDate: string;
}
