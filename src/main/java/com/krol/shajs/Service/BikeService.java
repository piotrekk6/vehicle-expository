package com.krol.shajs.Service;

import com.krol.shajs.Dto.BikeDto;

public interface BikeService {
    public void addBike(BikeDto bike);
}