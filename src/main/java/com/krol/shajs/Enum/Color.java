package com.krol.shajs.Enum;

public enum Color {
    RED,
    GREEN,
    BLUE,
    ORANGE,
    PURPLE,
    BLACK,
    BROWN,
    YELLOW,
}
